****mkcal.py - A script to generate my preferred one page calendar.****

Usage: > `python mkcal.py [YEAR]`

Example output: [https://intrepidhero.gitlab.io/mkcal/cal2020.html]

mkcal.py generates an HTML calendar with pleasing styling, including highlights 
for federal (US) holidays and horizontal bars showing biweekly pay periods. 

My normal use is to generate the years calendar as HTML and then either print 
a dead tree copy from Chrome or generate a PDF that I then crop and print. 
Someday I'll figure out how to go straight to PDF from python.

Feel free to fork and customize as you see fit but don't rely on my calculations
for anything important. Calendars are tricky.
