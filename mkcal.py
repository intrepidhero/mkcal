""" script to generate HTML calendars """

import calendar
import datetime as dt

### constants
week_day_header = "<tr>" + "".join(["<th>%s</th>" % f for f in ["M", "T", "W", "T", "F", "S", "S"]]) + "</tr>"

# I tried to use payperiodoffset() below, but it fell down in 2023. 
payperiod_start = {2014:0, 2015:0, 2016:0, 2017:0, 2018:1, 2019:1, 2020:1, 2021:1, 2022:1, 2023: 1, 2024: 0}

header = '''<html>\n<head>\n\t<link rel="stylesheet" type="text/css" href="cal.css">\n\t<title>Calendar</title></head>\n<body>\n'''
footer = '''</body>\n</html>'''

### functions
def closestweekday(h):
    if h.weekday() == 6:
        # sunday => Add a day
        h = h + dt.timedelta(1)
    elif h.weekday() == 5:
        # saturday
        h = h - dt.timedelta(1)
    return h

def nthweekday(year, month, n, day):
    """ day is the weekday 0=monday
        if you use -1 it will give you the last
    """
    if n == -1:
        n = 5
        d = (n-1)*7+1
        try:
            h = dt.date(year=year, month=month, day=d)
        except:
            n = 4
    d = (n-1)*7+1
    h = dt.date(year=year, month=month, day=d)
    if h.weekday() != day:
        daysforward = day - h.weekday()
        if daysforward < 1: 
            daysforward += 7
        h2 = h + dt.timedelta(daysforward)
        if h2.month != h.month:
            daysback = h.weekday() - day
            h = h - dt.timedelta(daysback)
        else:
            h = h2
    return h

def newyears(year):
    h = dt.date(year=year, month=1, day=1)
    return closestweekday(h)

def mlk(year):
    return nthweekday(year, 1, 3, 0)

def presidents(year):
    return nthweekday(year, 2, 3, 0)

def memorial(year):
    return nthweekday(year, 5, -1, 0)

def july4(year):
    h = dt.date(year=year, month=7, day=4)
    return closestweekday(h)

def labor(year):
    return nthweekday(year, 9, 1, 0)

def columbus(year):
    return nthweekday(year, 10, 2, 0)

def veterans(year):
    h = dt.date(year=year, month=11, day=11)
    return closestweekday(h)

def thanksgiving(year):
    return nthweekday(year, 11, 4, 3)

def christmas(year):
    h = dt.date(year=year, month=12, day=25)
    return closestweekday(h)

def holidays(year):
    theholidays = [
            newyears(year),
            mlk(year),
            presidents(year),
            memorial(year),
            july4(year),
            labor(year),
            columbus(year),
            veterans(year),
            thanksgiving(year),
            christmas(year)
    ]
    h = newyears(year+1)
    if h.year == year:
        theholidays.append(h)
    return theholidays

def wholeweeks(year1, year2):
    """ number of whole weeks between year1-01-01 and year2-01-01
    """
    td = dt.date(year1, 1, 1) - dt.date(year2, 1, 1)
    weeks = td.days / 7 - (year1 - year2) * 52
    return weeks

def payperiodoffset(year):
    """ calculates whether we start the year at the beginning or middle
    of a two week pay period
    returns 0 if year begins with two week pay period
    returns 1 if year begins with second half of pay period

    this is calibrated for my weeks that start with monday
    if you want to change that you would need to pick a different starting
    year that started with your chosen day (I think)
    """
    # whoops this broke in 2023 so back to the old way
    return payperiod_start[year]
    # My calendar would start mid-pay period in 2007-2012 and 2018-2023
    start = 1973
    weeks = wholeweeks(year, 1973)
    if weeks % 2 == 0:
        return 1
    else:
        return 0


### MAIN
def _build_cal(year=dt.datetime.today().year):
    print("The year of our Lord %d" % year)

    federal_holidays = holidays(year)

    c = calendar.Calendar()
    col3 = c.yeardatescalendar(year, 3)


    # table of months
    html = '''<table>\n'''
    html += '''<tr><td colspan="3"><h2 class="year">%d</h2></td></tr>\n''' % year

    payweek = payperiodoffset(year)
    thisweek = 0

    for row in col3:
        html += '''<tr>\n'''
        for month in row:
            html += '''<td>\n'''
            thismonth = month[1][0].month
            html += '''\t<h3 class="month">%s</h3>\n''' % month[1][0].strftime("%B")
            html += '''\t<table>\n\t\t''' + week_day_header+"\n"
            for week in month:
                if thisweek == payweek:
                    payweek += 2
                    html += '''\t\t<tr class="payperiodstart">'''
                else:
                    html += "\t\t<tr>"
                for day in week:
                    if thismonth != day.month:
                        html += '''<td class="day"></td>'''
                    else:
                        if day.weekday() == 6:
                            thisweek += 1
                        if day in federal_holidays:
                            print("Found holiday: %d/%d" % (day.month, day.day))
                            html += '''<td class="day holiday">%d</td>''' % day.day
                        else:
                            html += '''<td class="day">%d</td>''' % day.day
                html += "</tr>\n"

            html += '''\t</table>\n'''
            html += '''</td>\n'''

        html += '''</tr>\n'''
    html += '''</table>\n'''

    return html


def build_cal(filename, year=None):
    import os, shutil
    outfile = open(filename, "w")
    outfile.write(header)
    if year is None:
        outfile.write(_build_cal())
    else:
        outfile.write(_build_cal(year))
    outfile.write(footer)
    outfile.close()

    path = os.path.dirname(os.path.abspath(filename))
    csspath = os.path.dirname(os.path.abspath(__file__))
    """shutil.copy(os.path.join(csspath, "cal.css"), \
            os.path.join(path, "cal.css"))"""

if __name__ == "__main__":
    import sys
    if len(sys.argv) > 1:
        year = int(sys.argv[1])
    else:
        year = dt.datetime.utcnow().year
    build_cal("cal{}.html".format(year), year)
